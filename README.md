# WebRTC Screen recording with audio
Refer to https://screen-record-voice.glitch.me

## How to use?
* You should git clone this repository first.
```
docker-compose up -d
```
Viola! Then you can connect the starting container through https:/ip:8443
